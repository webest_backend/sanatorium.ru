<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class LoginController extends Controller
{
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginPortal(Request $request)
    {
        // todo: Добавить редирект на страницу с которой идёт запрос на авторизацию

        Auth::login(User::find(1), true);
        return redirect(route('personal'));
    }
}
