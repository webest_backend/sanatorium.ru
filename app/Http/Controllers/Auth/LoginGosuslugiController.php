<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class LoginGosuslugiController extends Controller
{
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        // todo: Добавить авторизацию через ГосУслуги
        // todo: Update or create user

        /**
         * Эмуляция авторизации через гос услуги
         */
        return view('caps.gosuslugi-auth');
    }
}
