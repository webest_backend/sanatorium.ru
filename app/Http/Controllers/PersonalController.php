<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PersonalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $status = $request->status;

        switch ($status) {
            case 'request':
                $link = route('personal', ['status' => 'success']);
                break;
            case 'problem':
                $link = 'https://do.gosuslugi.ru/appeals/new/?userSelectedRegion=19430000000&service=10002693843';
                break;
            default:
                $link = route('personal', ['status' => 'request']);
                break;
        }


        return view('pages.personal.index', compact('status', 'link'));
    }

    public function serviceStepTwo(Request $request)
    {
        $status = $request->status;

        switch ($status) {
            case 'success':
                $link = route('service-step-3');
                break;
            default:
                $link = route('service-step-2', ['status' => 'success']);
                break;
        }

        return view('pages.personal.step-2', compact('status', 'link'));
    }

    public function serviceStepThree(Request $request)
    {
        $status = $request->status;
        return view('pages.personal.step-3', compact('status'));
    }

    public function serviceStepFour(Request $request)
    {
        $status = $request->status;
        return view('pages.personal.step-4', compact('status'));
    }

    public function serviceStepFive()
    {
        return view('pages.personal.step-5');
    }
}
