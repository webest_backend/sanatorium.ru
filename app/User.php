<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];
    protected $appends = ['region_name', 'registration_address_polyclinics_name'];

    public function getRegionNameAttribute($region_id)
    {
        // todo: Запрос к базе синхронизации region_id => Информация о конкретном регионе
        return 'Ярославль';
    }

    public function getRegistrationAddressPolyclinicsNameAttribute($registration_address_polyclinics_id)
    {
        // todo: Запрос к базе синхронизации registration_address_polyclinics_id => Информация о привязанной поликлинике
        return 'г.Ярославль Поликлиника № 2';
    }
}
