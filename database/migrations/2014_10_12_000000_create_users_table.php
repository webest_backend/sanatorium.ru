<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_gosuslugi');
            $table->string('name');
            $table->string('surname');
            $table->string('patronymic');
            $table->string('snils');
            $table->date('birth_date');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->bigInteger('registration_address_polyclinics_id');
            $table->bigInteger('region_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
