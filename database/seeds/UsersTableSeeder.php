<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id_gosuslugi' => rand(0, 100),
            'name' => 'Сергей',
            'surname' => 'Петров',
            'patronymic' => 'Иванович',
            'snils' => '976-245-370 52',
            'birth_date' => '1970-10-10',
            'email' => str_random(10) . '@gmail.com',
            'phone' => '+79530000000',
            'registration_address_polyclinics_id' => rand(0, 100),
            'region_id' => rand(0, 100)
        ]);
    }
}
