/**
 * В проекте в качестве зависимости подключени Vue.js и Vuex,
 * для реализация сложных UI сценариев взаимодействия пользователя
 * с сервисом
 *
 * Подключени
 * - air-datepicker календарь
 * - magnific-popup модальные окна
 * - swiper - слайдер
 */

require("./bootstrap");
require("./helpers");

import { initApp } from "./init";
initApp();
