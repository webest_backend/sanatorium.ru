let formSender = {
    init() {
        this.eventHandler();
    },

    eventHandler() {
        let self = this;

        $(document).on("submit", ".js-form-sender", function() {
            self.sendRequest($(this));
            return false;
        });

        $(document).on("change", ".js-check-personal-data", function() {
            self.checkPersonalData($(this));
        });
    },

    sendRequest($form) {
        launchWindowPreloader();
        axios
            .post($form.attr("action"), $form.serialize())
            .then(function(response) {
                let is_error = response.data.error;
                let is_reset_form = !!$form.data("reset-form");
                let custom_modal_success = $form.data("modal-success");
                let is_refresh_page = $form.data("refresh-page");
                let msg = response.data.msg;

                if ($form.data("goal")) {
                    metrics.reachGoal($form.data("goal"));
                }

                $.magnificPopup.close();

                setTimeout(function() {
                    stopWindowPreloader();
                    if (is_error) {
                        messageError({ title: msg });
                        return;
                    }

                    if (is_refresh_page) location.reload();

                    if (is_reset_form) {
                        resetLabelInput($form);
                        $form[0].reset();
                    }

                    if (custom_modal_success) {
                        openModal(custom_modal_success);
                        return;
                    }

                    messageSuccess({ title: msg });
                }, 400);
            });
    },

    checkPersonalData($input) {
        let is_disabled = !$input.prop("checked");
        let $submit = $input.closest("form").find(".js-submit-form");

        if (is_disabled) {
            $submit.attr("disabled", "disabled");
        } else {
            $submit.removeAttr("disabled");
        }
    }
};

export { formSender };
