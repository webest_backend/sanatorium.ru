let homeStepNavigation = {
	init() {
		this.$menu = $(".js-home-step-navigation");

		if (this.$menu.length) {
			this.addHandlerScrollSections();
			this.eventHandler();
			this.setLeftOffsetFromNavigation();
			this.toggleFixedStepMenu();
			this.show_scroll = $(".js-home-step-navigation-wrapper").offset().top;
		}
	},

	eventHandler() {
		let self = this;

		$(document).on("click", ".js-home-step-navigation-link", function() {
			let anchor = $(this).attr("href");
			self.scrollStep(anchor);

			window.location.hash = anchor;
			setTimeout(() => {
				self.setActiveClassLink(anchor.replace("#", ""));
			}, 550);
			return false;
		});

		$(window).resize(() => {
			this.setLeftOffsetFromNavigation();
		});

		$(window).scroll(function() {
			self.toggleFixedStepMenu();
		});
	},

	setLeftOffsetFromNavigation() {
		this.$menu.css(
			"right",
			$(".js-home-step-navigation-wrapper").css("margin-left")
		);
	},

	toggleFixedStepMenu() {
		let current_scroll = $(window).scrollTop();

		if (current_scroll > this.show_scroll) {
			this.$menu.addClass("home-step-navigation--fixed");
		} else {
			this.$menu.removeClass("home-step-navigation--fixed");
		}
	},

	scrollStep(anchor) {
		if ($(anchor).length) scrollToElement($(anchor));
	},

	addHandlerScrollSections() {
		let self = this;
		dump($(".js-home-step-navigation-link"));
		$(".js-home-step-navigation-link").each(function() {
			let anchor = $(this).attr("href");
			anchor = anchor.replace("#", "");

			new Waypoint({
				element: $(".js-waypoint-" + anchor),
				offset: "30px",
				handler: function() {
					self.setActiveClassLink(anchor);
				}
			});
		});
	},

	setActiveClassLink(anchor) {
		let link = $('[href="#' + anchor + '"]');
		$(".js-home-step-navigation-link").removeClass(
			"home-step-navigation__link--active"
		);
		link.addClass("home-step-navigation__link--active");
	}
};

export { homeStepNavigation };
