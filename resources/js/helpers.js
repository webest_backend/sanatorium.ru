window.dump = value => {
	console.dir(value);
};

window.number_format = (number, decimals, dec_point, thousands_sep) => {
	number = (number + "").replace(/[^0-9+\-Ee.]/g, "");
	let n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = typeof thousands_sep === "undefined" ? " " : thousands_sep,
		dec = typeof dec_point === "undefined" ? "." : dec_point,
		s = "",
		toFixedFix = function(n, prec) {
			let k = Math.pow(10, prec);
			return "" + (Math.round(n * k) / k).toFixed(prec);
		};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : "" + Math.round(n)).split(".");
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || "").length < prec) {
		s[1] = s[1] || "";
		s[1] += new Array(prec - s[1].length + 1).join("0");
	}
	return s.join(dec);
};

window.makeId = () => {
	let text = "",
		possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (let i = 0; i < 8; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
};

window.getCookieValue = name => {
	let b = "; " + document.cookie;
	let c = b.split("; " + name + "=");
	return !!(c.length - 1)
		? c
				.pop()
				.split(";")
				.shift()
		: false;
};

window.setCookie = (name, value, expires, path, domain, secure) => {
	let today = new Date(),
		expires_default = new Date();
	expires_default.setDate(today.getDate() + 999999);

	expires = expires ? expires : expires_default;
	document.cookie =
		name +
		"=" +
		escape(value) +
		(expires ? "; expires=" + expires : "") +
		(path ? "; path=" + path : "") +
		(domain ? "; domain=" + domain : "") +
		(secure ? "; secure" : "");
};

window.getScrollWidth = () => {
	let div = document.createElement("div");

	div.style.overflowY = "scroll";
	div.style.width = "50px";
	div.style.height = "50px";

	// при display:none размеры нельзя узнать
	// нужно, чтобы элемент был видим,
	// visibility:hidden - можно, т.к. сохраняет геометрию
	div.style.visibility = "hidden";

	document.body.appendChild(div);
	let scrollWidth = div.offsetWidth - div.clientWidth;
	document.body.removeChild(div);

	return scrollWidth;
};

window.scrollToElement = (el = ".header") => {
	$("html, body").animate(
		{
			scrollTop: $(el).offset().top - 30
		},
		500
	);
};

window.checkComplexityPassword = password => {
	var s_letters = "qwertyuiopasdfghjklzxcvbnm"; // Буквы в нижнем регистре
	var b_letters = "QWERTYUIOPLKJHGFDSAZXCVBNM"; // Буквы в верхнем регистре
	var digits = "0123456789"; // Цифры
	var specials = "!@#$%^&*()_-+=|/.,:;[]{}"; // Спецсимволы
	var is_s = false; // Есть ли в пароле буквы в нижнем регистре
	var is_b = false; // Есть ли в пароле буквы в верхнем регистре
	var is_d = false; // Есть ли в пароле цифры
	var is_sp = false; // Есть ли в пароле спецсимволы
	for (var i = 0; i < password.length; i++) {
		/* Проверяем каждый символ пароля на принадлежность к тому или иному типу */
		if (!is_s && s_letters.indexOf(password[i]) != -1) is_s = true;
		else if (!is_b && b_letters.indexOf(password[i]) != -1) is_b = true;
		else if (!is_d && digits.indexOf(password[i]) != -1) is_d = true;
		else if (!is_sp && specials.indexOf(password[i]) != -1) is_sp = true;
	}
	var rating = 0;
	var classCss = "";
	if (is_s) rating++; // Если в пароле есть символы в нижнем регистре, то увеличиваем рейтинг сложности
	if (is_b) rating++; // Если в пароле есть символы в верхнем регистре, то увеличиваем рейтинг сложности
	if (is_d) rating++; // Если в пароле есть цифры, то увеличиваем рейтинг сложности
	if (is_sp) rating++; // Если в пароле есть спецсимволы, то увеличиваем рейтинг сложности
	/* Далее идёт анализ длины пароля и полученного рейтинга, и на основании этого готовится текстовое описание сложности пароля */
	if (password.length < 6 && rating < 3) classCss = "strength--red";
	else if (password.length < 6 && rating >= 3) classCss = "strength--yellow";
	else if (password.length >= 8 && rating < 3) classCss = "strength--yellow";
	else if (password.length >= 8 && rating >= 3) classCss = "strength--green";
	else if (password.length >= 6 && rating == 1) classCss = "strength--red";
	else if (password.length >= 6 && rating > 1 && rating < 4)
		classCss = "strength--yellow";
	else if (password.length >= 6 && rating == 4) classCss = "strength--green";
	return classCss;
};

window.launchWindowPreloader = () => {
	let html = `<div class="window-preloader js-window-preloader">
        <div class="window-preloader__loader">
            <img src="/img/color-icons/preloader.svg" alt="Circle Loader">
        </div>
    </div>`;
	$("body").append(html);

	setTimeout(function() {
		$(".js-window-preloader").addClass("window-preloader--show");
	}, 200);
};

window.stopWindowPreloader = (callback = null) => {
	$(".js-window-preloader").removeClass("window-preloader--show");
	setTimeout(function() {
		$(".js-window-preloader").remove();
		if (callback) {
			callback();
		}
	}, 300);
};

window.show_error_input = data => {
	let error = !!data.erorr ? data.erorr[0] : false,
		error_text = $(this).find(".input__error");

	if (error) {
		$(this).addClass("input__group--error");
		error_text.html(error);
	}
};

window.show_error_from_input = (form, data) => {
	let error = _.isArray(data.erorr) ? data.erorr[0] : data.erorr,
		$group = form.find('[data-input-group="' + data.key + '"]'),
		error_container = $group.find(".input__error");

	$group.addClass("input__group--error");
	if (error && error_container.data("not-replace") != "Y") {
		error_container.html(error);
	}
};

window.scroll_to_error = form => {
	let error = form.find(".input__group--error");
	if (!!$(error)) {
		$("html, body").animate({ scrollTop: $(error).offset().top - 50 }, 500);
	}
};

window.clear_error_input = form => {
	form.find(".input__group").each(function() {
		$(this).removeClass("input__group--error");
	});

	form.find(".input__mit-select").each(function() {
		$(this).removeClass("input__group--error");
	});

	if (form.has(".input__group")) {
		form.removeClass("input__group--error");
	}
};

window.showDevelopers = () => {
	function Person(name, development, contact) {
		this.Name = name;
		this.Development = development;
		this.Contact = contact;
	}

	let data = [
		new Person("Василий Коркин", "front-end", "v.ivan.korkin@gmail.com")
	];

	console.table(data);
};
