import { formSender } from "./handlers/form-sender";
import { homeStepNavigation } from "./handlers/home-step-navigation";

function initApp() {
	formSender.init();
	homeStepNavigation.init();
}

export { initApp };
