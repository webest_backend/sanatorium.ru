@extends('layouts.caps')

@section('content')
    <div class="container cap-container">
        <div class="sub-title">Эмуляция авторизации через гос услуги</div>
        <p>
            Получаем информацию о пользователе через сторонний сервис
        </p>
        <p>
            Перенаправляем на маршрут портала для обработки информации о пользователе
        </p>
        <a href="{{route('portal-login')}}" class="link">Перенаправление</a>
    </div>
    
@endsection
