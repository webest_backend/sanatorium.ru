@extends('layouts.app')

@section('content')
    <div class="home-banner">
        @include('parts.menu-navigation', ['modifier' => 'menu-navigation__wrapper--white'])

        <div class="container">
            <div class="home-banner__title">
                <h1 class="seo-title">
                    Бесплатные путевки
                    <br>
                    в санаторно-курортные организации
                </h1>
            </div>
            <div class="information-cards">
                <div class="information-card">
                    <div class="information-card__content">
                        <div class="information-card__action">
                            <div class="information-card__name">
                                Личный кабинет
                            </div>
                            <a href="{{route('personal')}}" class="btn btn--border">
                                Войти
                            </a>
                        </div>
                    </div>
                    <div class="information-card__image"
                         style="background-image: url('/img/information-card-image-1.png');"></div>
                </div>
                <a href="{{route('check-queue')}}" class="information-card">
                        <span class="information-card__content">
                        <span class="information-card__name">Проверить очередь для получения путевки</span></span>
                    <span class="information-card__image"
                          style="background-image: url('/img/information-card-image-2.png');"></span>
                </a>
                <div class="information-card">
                    <div class="information-card__content">
                        <div class="information-card__counter">
                            <div class="information-card__count">
                                103
                            </div>
                            <div class="information-card__name">
                                путевки выдали в этом году
                            </div>
                        </div>
                    </div>
                    <div class="information-card__image"
                         style="background-image: url('/img/information-card-image-3.png');"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="home-steps">
        <div class="home-step-navigation__wrapper container js-home-step-navigation-wrapper">
            <div class="home-step-navigation js-home-step-navigation">
                <div class="home-step-navigation__list">
                    <a href="#step-1"
                       class="home-step-navigation__link home-step-navigation__link--active js-home-step-navigation-link">
                        Кто может получить бесплатные путевки на санаторно-курортное лечение?
                    </a>
                    <a href="#step-2" class="home-step-navigation__link js-home-step-navigation-link">
                        Как встать в очередь на получение путевки?
                    </a>
                    <a href="#step-3" class="home-step-navigation__link js-home-step-navigation-link">
                        Очередь на получение путевки
                    </a>
                    <a href="#step-4" class="home-step-navigation__link js-home-step-navigation-link">
                        Получение путевки, когда подойдет очередь
                    </a>
                    <a href="#step-5" class="home-step-navigation__link js-home-step-navigation-link">
                        Что входит в путевку?
                    </a>
                    <a href="#step-6" class="home-step-navigation__link js-home-step-navigation-link">
                        Какие документы нужно получить в санатории?
                    </a>
                </div>
                <a href="{{route('personal')}}" class="btn">
                    Получить услугу
                </a>
            </div>
        </div>
        <div class="section-gray js-waypoint-step-1" id="step-1">
            <div class="home-step container">
                <div class="home-step__content">
                    <div class="home-step__title sub-title">
                        1. Кто может получить бесплатные путевки
                        <br>
                        на санаторно-курортное лечение?
                    </div>
                    <div class="home-step__description text-content">
                        <p>
                            Право на бесплатное санаторно-курортное лечение может быть предоставлено гражданам
                            различных
                            льготных категорий как федерального, так и регионального уровня.
                        </p>
                        <p>
                            Вы можете узнать попадаете ли Вы в категорию имеющих право бесплатного посещения
                            санатория.
                        </p>
                        <strong>
                            Бесплатные путевки на санаторно-курортное лечение могут получить:
                        </strong>
                    </div>
                    <ul class="ul-arrow">
                        <li>
                            инвалиды и ветераны войны, ветераны боевых действий
                        </li>
                        <li>
                            бывшие несовершеннолетние узники концлагерей, гетто, других мест принудительного
                            содержания, созданных фашистами и их союзниками в период Второй мировой войны;
                        </li>
                        <li>
                            члены семей погибших (умерших) инвалидов и участников Великой Отечественной войны,
                            инвалидов и ветеранов боевых действий;
                        </li>
                        <li>
                            граждане, пострадавшие от воздействия радиации;
                        </li>
                        <li>
                            инвалиды и дети-инвалиды;
                        </li>
                        <li>
                            неработающие пенсионеры и граждане предпенсионного возраста;
                        </li>
                        <li>
                            неработающие ветераны труда (ветераны военной службы), труженики тыла, реабилитированные
                            лица и граждане, признанные пострадавшими от политических репрессий;
                        </li>
                        <li>
                            жертвы террористических актов, а также супруги, родители и дети жертв террористических
                            актов;
                        </li>
                        <li>
                            граждане, награжденные нагрудным знаком «Почетный донор России» или нагрудным знаком
                            «Почетный донор СССР».
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="home-step container js-waypoint-step-2" id="step-2">
            <div class="home-step__content">
                <div class="home-step__title sub-title">
                    2. Как встать в очередь на получение путевки?
                </div>
                <div class="home-step__description text-content">
                    <p>
                        Чтобы встать в очередь на получение санаторно-курортной путевки, вам понадобятся собрать ряд
                        документов и предоставить их в Фонд Социального Страхования
                    </p>
                    <strong>
                        Чтобы встать в очередь на получение санаторно-курортной путевки, вам понадобятся:
                    </strong>
                </div>
                <ul class="ul-icon">
                    <li class="ul-icon__top">
                        Медицинская справка для получения путевки на санаторно-курортное лечение формы № 070/у,
                        подтверждающая, что обратившийся нуждается в санаторно-курортном лечении
                        <a href="{{route('personal')}}" class="btn">Записаться на прием</a>
                    </li>
                    <li>
                        Заявление в ФСС, в котором указываете желаемое время посещения санатория и другие важные для
                        вас детали. Далее оно уйдет на рассмотрение. В течение 20 дней вам сообщат о принятом
                        решении.
                    </li>
                    <li>
                        Паспорт или другой документ, удостоверяющий личность обратившегося
                    </li>
                    <li>
                        Трудовая книжка для подтверждения факта увольнения с работы (в случае обращения
                        неработающего пенсионера). В случае отсутствия трудовой книжки — справка с последнего места
                        работы (службы), заверенная в установленном порядке, подтверждающая факт увольнения
                    </li>
                    <li>
                        Пенсионное удостоверение
                    </li>
                </ul>
            </div>
        </div>
        <div class="section-gray js-waypoint-step-3" id="step-3">
            <div class="home-step container">
                <div class="home-step__content">
                    <div class="home-step__title sub-title">
                        3. Ожидание очереди на получение путевки
                    </div>
                    <div class="home-step__description text-content">
                        <ul class="ul-arrow">
                            <li>
                                После подачи заявления и необходимых документов в ФСС Вы будете поставлены в очеред
                                ожидания путевки.
                            </li>
                            <li>
                                Путевки на бесплатное санаторно-курортное лечение выдаются в порядке общей очереди,
                                но
                                есть несколько категорий граждан, которые могут получить путевки в первую очередь.
                            </li>
                            <li>
                                Инвалидам I группы и детям-инвалидам, а также гражданам, получившим повреждения
                                здоровья
                                в результате террористических актов, и детям в возрасте до 18 лет погибших (умерших)
                                в
                                результате террористических актов дополнительно выдается санаторно-курортная путевка
                                для
                                сопровождающего их лица.
                            </li>
                        </ul>
                        <strong>
                            Вы можете проверить ваше номер в очереди на нашем сайте после прохождения предыдущих
                            шагов.
                        </strong>
                    </div>
                </div>
            </div>
        </div>
        <div class="home-step container js-waypoint-step-4" id="step-4">
            <div class="home-step__content">
                <div class="home-step__title sub-title">
                    4. Получение путевки, когда подойдет очередь
                </div>
                <div class="home-step__description text-content">
                    <ul class="ul-icon">
                        <li>
                            Когда подойдет ваша очередь на получение санаторно-курортной путевки, вам придет
                            оповещение. После этого обратитесь в ФСС.
                        </li>
                        <li>
                            Выдача путевки производится не ранее чем за месяц до даты заезда в санаторий, указанной
                            в путевке.
                        </li>
                        <li>
                            После получения путевки вам нужно будет обратиться к участковому терапевту для
                            проведения дополнительного обследования и получения санаторно-курортной карты.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="section-gray js-waypoint-step-5" id="step-5">
            <div class="home-step container">
                <div class="home-step__content">
                    <div class="home-step__title sub-title">
                        5. Что входит в путевку?
                    </div>
                    <div class="home-step__description text-content">
                        <p>
                            По условиям государственных контрактов, заключенных со здравницами по итогам конкурсных
                            процедур, в стоимость путевок, выдаваемых гражданам льготных категорий, включено:
                        </p>
                        <ul class="ul-arrow" style="padding: 20px 0;">
                            <li>
                                проживание в номерах с площадью не менее 6 кв.м. на одно койко-место, в условиях,
                                удовлетворяющих действующим санитарным правилам, нормам и гигиеническим нормам, при
                                строгом соблюдении правил пожарной безопасности
                            </li>
                            <li>
                                диетическое и лечебное питание в соответствии с медицинскими показаниями не менее 5
                                раз день по заказному меню с учетом системы стандартных диет (обязательных: завтрак,
                                обед, полдник, ужин, кефир)
                            </li>
                            <li>
                                культурно-просветительный досуг
                            </li>
                        </ul>
                        <p>
                            Виды и количество медицинских услуг и лечебных процедур в каждом конкретном случае
                            определяются лечащим врачом здравницы с учетом состояния здоровья отдыхающего, имеющихся
                            медицинских показаний и противопоказаний.
                        </p>
                        <strong>
                            <a href="#" class="link">Список санаториев</a>
                        </strong>
                    </div>
                </div>
            </div>
        </div>
        <div class="home-step container js-waypoint-step-6" id="step-6">
            <div class="home-step__content">
                <div class="home-step__title sub-title">
                    6. Какие документы нужно получить в санатории?
                </div>
                <div class="home-step__description text-content">
                    <ul class="ul-icon">
                        <li>
                            После завершения санаторно-курортного лечения вам должны выдать обратный талон
                            санаторно-курортной карты и обратный талон к путевке с отметкой о пребывании в
                            санатории.
                        </li>
                        <li>
                            Вы обязаны предоставить обратный талон санаторно-курортной карты в поликлинику, выдавшую
                            эту карту, а обратный талон к путевке с отметкой о пребывании в санатории — в любой из
                            центров госуслуг «Мои документы», где получили путевку, в течение месяца после
                            возвращения.
                        </li>
                        <li>
                            Законные представители детей, получивших санаторно-курортное лечение по медицинским
                            показаниям, возвращают весь комплект документов, полученных в санатории, лечащему врачу
                            (педиатру) в поликлинику по месту жительства.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="section-gray">
        <div class="container">
            <div class="section-questions">
                <div class="section-questions__content">
                    <div class="sub-title">
                        Не нашли ответа на свой вопрос?
                    </div>
                    <p>Вы моженте задать нам вопрос онлайн</p>
                    <button class="btn">Задать вопрос</button>
                </div>
                <div class="section-questions__image">
                    <img src="/img/question-image.png" alt="Не нашли ответа на свой вопрос?">
                </div>
            </div>
        </div>
    </div>
@endsection
