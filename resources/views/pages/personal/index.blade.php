@extends('layouts.personal')

@section('content')
    <div class="container">
        <div class="bread-crumbs">
            <a href="/" class="bread-crumb" title="Главная">Главная</a>
            <div class="bread-crumb">Личный кабинет</div>
        </div>
    </div>
    @include('pages.personal.parts.user-small-info')
    <div class="container">
        <div class="service-steps">
            <a href="{{route('personal')}}" class="service-step service-step--active">Шаг №1</a>
            <a href="{{route('service-step-2')}}" class="service-step service-step--disabled">Шаг №2</a>
            <a href="{{route('service-step-3')}}" class="service-step service-step--disabled">Шаг №3</a>
            <a href="{{route('service-step-4')}}" class="service-step service-step--disabled">Шаг №4</a>
            <a href="{{route('service-step-5')}}" class="service-step service-step--disabled">Шаг №5</a>
        </div>
    </div>
    <div class="section-gray">
        <div class="container">
            @if($status != 'provided')
                <div class="personal-service">
                    <div class="personal-service__title">
                        Чтобы встать в очередь на получение санаторно-курортной путевки, вам понадобятся:
                    </div>
                    <div class="personal-service__content">
                        <ul class="ul-icon">
                            <li class="ul-icon__top">
                                Медицинская справка для получения путевки на санаторно-курортное лечение формы № 070/у,
                                подтверждающая, что обратившийся нуждается в санаторно-курортном лечении
                                @if(!$status)
                                    <a href="{{$link}}" class="btn">Записаться на прием</a>
                                @endif
                            </li>
                        </ul>
                    </div>
                    @if($status)
                        @if($status == 'request')
                            <div class="service-status">
                                <div class="service-status__icon">
                                    <img src="/img/color-icons/default.svg">
                                </div>
                                <div class="service-status__content">
                                    Статус: Записан на прием 10 августа 2019 Поликлиника №2
                                </div>
                            </div>
                            @if($status == 'request')
                                <div class="cap-content">
                                    <a href="{{$link}}" class="link">Эмуляции приём у терапевта (Успешно)</a>
                                    <a href="{{route('personal', ['status' => 'problem'])}}" class="link">Эмуляции приём
                                        у
                                        терапевта (Отказ)</a>
                                </div>
                            @endif
                        @endif
                        @if($status == 'problem')
                            <div class="service-status service-status--problem">
                                <div class="service-status__icon">
                                    <img src="/img/color-icons/problem.svg">
                                </div>
                                <div class="service-status__content">
                                    Статус: Вам отказано в предоставлении услуги.
                                    <a href="{{$link}}" class="link" target="_blank">Обжаловать решение</a>
                                </div>
                            </div>
                        @endif
                        @if($status == 'success')
                            <div class="service-status service-status--success">
                                <div class="service-status__icon">
                                    <img src="/img/color-icons/success.svg">
                                </div>
                                <div class="service-status__content">
                                    Статус: Вы поставлены на учет
                                </div>
                                <a href="{{route('service-step-2')}}" class="next-step">
                                    <span class="next-step__title">Следующий шаг</span>
                                    <span class="next-step__icon">
                                    <svg class="icon icon-9">
                                        <use xlink:href="#9"></use>
                                    </svg>
                                </span>
                                </a>
                            </div>
                        @endif
                    @endif
                </div>
            @else
                <div class="personal-service">
                    <div class="question-cards" style="margin-top: 0;">
                        <div class="question-card" style="width: 457px;">
                            <div class="question-card__title">
                                Вы сможете повторно воспользоваться услугой:
                            </div>
                            <div class="question-card__value" style="color: #53CEBF; font-size: 15px;">
                                25 ноября 2020
                            </div>
                        </div>
                    </div>
                    <div class="remained-day">
                        <span>Осталось:</span>
                        <div class="remained-day__value">
                            365
                        </div>
                        <span>дней</span>
                    </div>
                    <div class="cap-content">
                        <a href="{{route('personal')}}" class="link">
                            Базовое состояние личного кабинета
                        </a>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
