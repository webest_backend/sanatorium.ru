<div class="service-status">
    <div class="service-status__icon">
        <img src="/img/color-icons/default.svg">
    </div>
    <div class="service-status__content">
        Статус: Записан на прием 10 августа 2019 Поликлиника №2
    </div>
</div>
<div class="service-status service-status--success">
    <div class="service-status__icon">
        <img src="/img/color-icons/success.svg">
    </div>
    <div class="service-status__content">
        Статус: Вы поставлены на учет
    </div>
    <a href="{{route('service-step-2')}}" class="next-step">
        <span class="next-step__title">Следующий шаг</span>
        <span class="next-step__icon"></span>
    </a>
</div>
<div class="service-status service-status--problem">
    <div class="service-status__icon">
        <img src="/img/color-icons/problem.svg">
    </div>
    <div class="service-status__content">
        Статус: Вам отказано в предоставлении услуги. <a href="#" class="link">Обжаловать решение</a>
    </div>
</div>