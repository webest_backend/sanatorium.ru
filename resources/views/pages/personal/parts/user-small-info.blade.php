<div class="container">
    <div class="user-small">
        <div class="user-small__photo" style="background-image: url('/img/photo.png');"></div>
        <div class="user-small__content">
            <div class="user-small__name">
                Демидов Павел Григорьевич
            </div>
            <a href="#" class="link">Редактировать данные</a>
        </div>
    </div>
</div>