@extends('layouts.personal')

@section('content')
    <div class="container">
        <div class="bread-crumbs">
            <a href="/" class="bread-crumb" title="Главная">Главная</a>
            <div class="bread-crumb">Личный кабинет</div>
        </div>
    </div>
    @include('pages.personal.parts.user-small-info')
    <div class="container">
        <div class="service-steps">
            <a href="{{route('personal', ['status' => 'success'])}}" class="service-step service-step--passed">Шаг
                №1</a>
            <a href="{{route('service-step-2')}}" class="service-step service-step--active">Шаг №2</a>
            <a href="{{route('service-step-3')}}" class="service-step service-step--disabled">Шаг №3</a>
            <a href="{{route('service-step-4')}}" class="service-step service-step--disabled">Шаг №4</a>
            <a href="{{route('service-step-5')}}" class="service-step service-step--disabled">Шаг №5</a>
        </div>
    </div>
    <div class="section-gray">
        <div class="container">
            <div class="personal-service">
                <div class="personal-service__title">
                    Постановка в очередь на получение услуги:
                </div>
                @if(!$status || $status != 'success')
                <div class="personal-service__content">
                    <ul class="ul-icon">
                        <li class="ul-icon__top">
                            Предоставьте документы в ФСС. Скачайте и заполнике заявление в ФСС
                            <a href="/doc/Образец-заявление-на-СКЛ.doc" class="link" download="Образец заявление на СКЛ">Скачать бланк</a>
                            @if(!$status)
                                <a href="{{$link}}" class="btn">Получить услугу</a>
                            @endif
                        </li>
                    </ul>
                </div>
                @endif
                @if($status)
                    @if($status == 'success')
                        <div class="service-status service-status--success">
                            <div class="service-status__icon">
                                <img src="/img/color-icons/success.svg">
                            </div>
                            <div class="service-status__content">
                                Статус: Вы поставлены в очередь. Ваш номер 24
                            </div>
                            <a href="{{route('service-step-3')}}" class="next-step">
                                <span class="next-step__title">Следующий шаг</span>
                                <span class="next-step__icon">
                                    <svg class="icon icon-9">
                                        <use xlink:href="#9"></use>
                                    </svg>
                                </span>
                            </a>
                        </div>
                    @endif
                @endif

            </div>
        </div>
    </div>
@endsection
