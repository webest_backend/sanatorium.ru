@extends('layouts.personal')

@section('content')
    <div class="container">
        <div class="bread-crumbs">
            <a href="/" class="bread-crumb" title="Главная">Главная</a>
            <div class="bread-crumb">Личный кабинет</div>
        </div>
    </div>
    @include('pages.personal.parts.user-small-info')
    <div class="container">
        <div class="service-steps">
            <a href="{{route('personal', ['status' => 'success'])}}" class="service-step">Шаг №1</a>
            <a href="{{route('service-step-2', ['status' => 'success'])}}" class="service-step service-step--passed">Шаг
                №2</a>
            <a href="{{route('service-step-3')}}" class="service-step service-step--active">Шаг №3</a>
            <a href="{{route('service-step-4')}}" class="service-step service-step--disabled">Шаг №4</a>
            <a href="{{route('service-step-5')}}" class="service-step service-step--disabled">Шаг №5</a>
        </div>
    </div>
    <div class="section-gray">
        <div class="container">
            <div class="personal-service">
                @if($status == 'problem')
                    <div class="personal-service__content" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);margin-bottom: 40px;">
                        <div class="service-status service-status--problem" style="margin-top: 0;">
                            <div class="service-status__icon">
                                <img src="/img/color-icons/problem.svg">
                            </div>
                            <div class="service-status__content">
                                Ваша справка формы № 070/у устарела.
                            </div>
                        </div>
                        <ul class="ul-icon">
                            <li class="ul-icon__top">
                                Медицинская справка для получения путевки на санаторно-курортное лечение формы № 070/у,
                                подтверждающая, что обратившийся нуждается в санаторно-курортном лечении
                                <a class="btn" href="https://www.gosuslugi.ru/10066/1?from=main" target="_blank">Записаться на прием</a>
                            </li>
                        </ul>
                    </div>
                @endif
                <div class="personal-service__title">
                    Ожидание очереди:
                </div>
                <div class="personal-service__content">
                    <div class="question-cards">
                        <div class="question-card">
                            <div class="question-card__title">
                                Ваш номер в очереди:
                            </div>
                            <div class="question-card__value">
                                № 125
                            </div>
                        </div>
                        <div class="question-card question-card--green">
                            <div class="question-card__title">
                                Прогноз даты получения путевки:
                            </div>
                            <div class="question-card__value">
                                Ноябрь 2019
                            </div>
                        </div>
                    </div>

                    <div class="notification-settings">
                        <div class="personal-service__title">
                            Настройка уведомлений о продвижении в очереди:
                        </div>
                        <div class="notification-settings__header">
                            <div class="notification-settings__title">
                                Уведомить за
                            </div>
                            <button class="notification-settings__period">
                                <span class="notification-settings__period-name">
                                    1 месяц
                                </span>
                            </button>
                        </div>
                        <div class="notification-setting">
                            <div class="notification-setting__name">
                                Push-уведомления
                            </div>
                            <div class="notification-setting__change">
                                <a href="#" class="link">Изменить</a>
                            </div>
                        </div>
                        <div class="notification-setting">
                            <div class="notification-setting__name">
                                SMS-уведомления
                            </div>
                            <div class="notification-setting__change">
                                <a href="#" class="link">Изменить</a>
                            </div>
                        </div>
                        <div class="notification-setting">
                            <div class="notification-setting__name">
                                E-mail-уведомления
                            </div>
                            <div class="notification-setting__change">
                                <a href="#" class="link">Изменить</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cap-content">
                    <a href="{{route('service-step-3', ['status' => 'problem'])}}" class="link">Эмуляции события "Ваша
                        справка формы № 070/у устарела."</a>
                    <a href="{{route('service-step-4')}}" class="link">Эмуляции события: Подошла очередь на получение
                        путёвки</a>
                </div>
            </div>
        </div>
    </div>
@endsection
