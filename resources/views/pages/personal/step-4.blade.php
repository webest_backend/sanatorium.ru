@extends('layouts.personal')

@section('content')
    <div class="container">
        <div class="bread-crumbs">
            <a href="/" class="bread-crumb" title="Главная">Главная</a>
            <div class="bread-crumb">Личный кабинет</div>
        </div>
    </div>
    @include('pages.personal.parts.user-small-info')
    <div class="container">
        <div class="service-steps">
            <a href="{{route('personal', ['status' => 'success'])}}" class="service-step service-step--passed">Шаг
                №1</a>
            <a href="{{route('service-step-2', ['status' => 'success'])}}" class="service-step service-step--passed">Шаг
                №2</a>
            <a href="{{route('service-step-3')}}" class="service-step service-step--passed">Шаг №3</a>
            <a href="{{route('service-step-4')}}" class="service-step service-step--active">Шаг №4</a>
            <a href="{{route('service-step-5')}}" class="service-step service-step--disabled">Шаг №5</a>
        </div>
    </div>
    <div class="section-gray">
        <div class="container">
            <div class="personal-service">
                @if(!$status)
                    <div class="personal-service__title">
                        Ваша очередь подошла, вам понадобятся:
                    </div>
                    <div class="personal-service__content">
                        <ul class="ul-icon">
                            <li class="ul-icon__top">
                                Вам нужно обратиться к участковому терапевту для проведения
                                дополнительного обследования и получения санаторно-курортной карты.
                                <a class="btn" href="{{route('service-step-4', ['status' => 'success'])}}">
                                    Записаться на прием
                                </a>
                            </li>
                        </ul>
                    </div>
                @endif
                @if($status == 'success')
                    <div class="personal-service__title">
                        Ваша очередь подошла:
                    </div>
                    <div class="service-status service-status--success">
                        <div class="service-status__icon">
                            <img src="/img/color-icons/success.svg">
                        </div>
                        <div class="service-status__content">
                            Статус: Санаторно курортная карта выдана
                        </div>
                        <a href="{{route('service-step-5')}}" class="next-step">
                            <span class="next-step__title">Следующий шаг</span>
                            <span class="next-step__icon">
                                    <svg class="icon icon-9">
                                        <use xlink:href="#9"></use>
                                    </svg>
                                </span>
                        </a>
                    </div>
                    <div class="question-cards">
                        <div class="question-card">
                            <div class="question-card__title">
                                Дата прибытия:
                            </div>
                            <div class="question-card__value">
                                15 ноября 2019
                            </div>
                        </div>
                        <div class="question-card">
                            <div class="question-card__title">
                                Дата окончания:
                            </div>
                            <div class="question-card__value">
                                25 ноября 2019
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
