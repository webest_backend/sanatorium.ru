@extends('layouts.personal')

@section('content')
    <div class="container">
        <div class="bread-crumbs">
            <a href="/" class="bread-crumb" title="Главная">Главная</a>
            <div class="bread-crumb">Личный кабинет</div>
        </div>
    </div>
    @include('pages.personal.parts.user-small-info')
    <div class="container">
        <div class="service-steps">
            <a href="{{route('personal', ['status' => 'success'])}}" class="service-step service-step--passed">Шаг
                №1</a>
            <a href="{{route('service-step-2', ['status' => 'success'])}}" class="service-step service-step--passed">Шаг
                №2</a>
            <a href="{{route('service-step-3')}}" class="service-step service-step--passed">Шаг №3</a>
            <a href="{{route('service-step-4', ['status' => 'success'])}}" class="service-step service-step--passed">Шаг
                №4</a>
            <a href="{{route('service-step-5')}}" class="service-step service-step--active">Шаг №5</a>
        </div>
    </div>
    <div class="section-gray">
        <div class="container">
            <div class="personal-service">
                <div class="personal-service__title">
                    Какие документы нужно получить в санатории?
                </div>
                <div class="personal-service__content">
                    <ul class="ul-icon">
                        <li style="border-bottom: 1px solid rgba(0, 0, 0, 0.15)">
                            После завершения санаторно-курортного лечения вам должны выдать обратный талон
                            санаторно-курортной карты и обратный талон к путевке с отметкой о пребывании в санатории.
                        </li>
                        <li class="ul-icon__top">
                            Вы обязаны предоставить обратный талон санаторно-курортной карты в поликлинику, выдавшую эту карту.
                            <a class="btn" href="#">Разрешить передачу данных</a>
                        </li>
                    </ul>
                </div>
                <div class="cap-content">
                    <a href="{{route('personal', ['status' => 'provided'])}}" class="link">
                        Эмуляции состояния первого шага в личном кабинете после оказания услуги в полном объёме
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
