<footer class="footer">
    <div class="footer__content container">
        <div class="footer__top">
            <div class="footer__contact">
                <a href="tel:" class="footer__phone" title="Телефон поддержки">8 800 125 154</a>
                <span>Телефон поддержки</span>
            </div>
            <div class="footer__applications">
                <a href="#">
                    <img src="/img/google.svg" alt="Google Play">
                </a>
                <a href="#">
                    <img src="/img/apple.svg" alt="App Store">
                </a>
            </div>

        </div>
        <div class="footer__bottom">
            <div class="footer__copy">
                {{date("Y")}} Sanatorium.gov
            </div>
            <a href="#">Карта сайта</a>
        </div>
    </div>
</footer>
<button class="write-message-bot">
    <span class="write-message-bot__icon">
        <svg class="icon icon-10">
            <use xlink:href="#10"></use>
        </svg>
    </span>
    <span class="write-message-bot__title">Задайте свой вопрос</span>
    <span class="write-message-bot__close"></span>
</button>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>