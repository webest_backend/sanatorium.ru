<?
$modifier = isset($modifier) ? $modifier : "";
?>

<div class="menu-navigation__wrapper {{$modifier}}">
    <div class="menu-navigation container">
        <a href="{{route('home')}}" class="logo">
            <span class="logo__icon">
                <svg class="icon icon-logo">
                    <use xlink:href="#logo"></use>
                </svg>
            </span>
            <span class="logo__content">
                <span class="logo__name">Санаториум</span>
                <span class="logo__description">Ваш отдых - ваше долголетие</span>
            </span>
        </a>
        <div class="menu-links">
            <a href="#" class="menu-link" title="Список санаториев">Список санаториев</a>
            <a href="{{route('check-queue')}}" class="menu-link" title="Проверить очередь">Проверить очередь</a>
        </div>
    </div>
</div>