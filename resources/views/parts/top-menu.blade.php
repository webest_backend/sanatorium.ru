<header class="header">
    <div class="top-menu__wrapper">
        <div class="top-menu container">
            <a href="#" class="top-menu__more">
                <span class="top-menu__more-icon">
                    <img src="/img/color-icons/more-link.svg" alt="Другие сайты государства">
                </span>
                <span class="top-menu__more-name">
                Другие сайты государства
            </span>
            </a>
            <div class="top-menu__additional">
                <a href="#" class="menu-search">
                    <span class="menu-search__icon">
                        <svg class="icon icon-2">
                            <use xlink:href="#2"></use>
                        </svg>
                    </span>
                    <span class="menu-search__text">Поиск</span>
                </a>
                <button class="region-select">
                    <span class="region-select__icon">
                        <svg class="icon icon-marker">
                            <use xlink:href="#marker"></use>
                        </svg>
                    </span>
                    <span class="region-select__name">Ярославль</span>
                </button>
                <a href="{{route('personal')}}" class="top-personal">
                <span class="top-personal__icon">
                    <svg class="icon icon-1">
                        <use xlink:href="#1"></use>
                    </svg>
                </span>
                    <span class="top-personal__name">Личный кабинет</span>
                </a>
                <a href="#" class="visually-impaired-link">
                <span class="visually-impaired-link__icon">
                    <svg class="icon icon-3">
                        <use xlink:href="#3"></use>
                    </svg>
                </span>
                    <span class="visually-impaired-link__name">Версия для слабовидящих</span>
                </a>
            </div>
        </div>
    </div>
</header>