<?php

Route::get('/', 'HomeController@index')->name('home');


Route::group(['prefix' => 'auth'], function () {
    Route::get('/login', 'Auth\LoginGosuslugiController@login')->name('login');
    Route::get('/portal-login', 'Auth\LoginController@loginPortal')->name('portal-login');
    Route::get('/logout', 'Auth\LogoutController@logout')->name('logout');
});

Route::group(['prefix' => 'personal'], function () {
    Route::get('/', "PersonalController@index")->name('personal');
    Route::get('/step-2', "PersonalController@serviceStepTwo")->name('service-step-2');
    Route::get('/step-3', "PersonalController@serviceStepThree")->name('service-step-3');
    Route::get('/step-4', "PersonalController@serviceStepFour")->name('service-step-4');
    Route::get('/step-5', "PersonalController@serviceStepFive")->name('service-step-5');
});

Route::get('/check-queue', function () {
    return view('caps.check-queue');
})->name("check-queue");